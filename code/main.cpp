#include <iostream>
#include <string>
#include "FileStructure.h"
#include "Key.h"
#include "Value.h"
#include "Sort.h"
#include <stdexcept>


int main(int argc, char** argv)
{
    bool printhead = false;
    if(argc > 1)
    {
        std::string arg1(argv[1]);
        if (arg1.compare("print") == 0)
        {
            printhead = true;
        }
        else if (arg1.compare("help") == 0)
        {
            std :: cout << std :: endl
                        << " ./sort [param1]                to run sort regularly." << std :: endl
                        << " time ./sort [param1]           to get the run time." << std :: endl
                        << " valgrind ./sort [param1]       to check if all memory is released on exit." << std :: endl
                        << std :: endl
                        << " param1 can be 'print' or 'help'." << std :: endl
                        << std :: endl
                        << "  -- 'help'                     shows this info." << std :: endl
                        << "  -- 'print'                    makes sort print the entire sorted list to the terminal." << std :: endl
                        << "  -- 'print' >> [text.txt]      In combination to 'print' >> [filename.txt] can be used" << std :: endl 
                        << "                                  to print the sorted list to file." << std :: endl
                        << "                                  The entire command would be './sort print >> textfile.txt'" << std :: endl
                        << std :: endl
                        << std :: endl
                        << std :: endl
                        << std :: endl
                        << " sort will not be executed, since you only asked for help." << std :: endl
                        << std :: endl;
            return (0);
        }
    }

    FileStructure f;
    Sort s;

    Key* head = new Key();
    Key* end = head;
    
    f.loadFile("data/gibberish.bin", *head);
    
    int length = s.getLength(head, &end);
    head = s.sort(head, end, length);

    Key* temp = head;
    do 
    {
        Value* vhead = temp->getValuePtr();
        Value* vend = vhead;
        
        int vlength = s.getLength(vhead, &vend);
        vhead = s.sort(vhead, vend, vlength);
        
        temp->setValuePtr(vhead);
        temp = temp->getNext();
    } while (temp != NULL);
    if (printhead)
    {
        head->print();
    }
    f.saveFile(*head, "sorted.bin");
    delete head;
    return 0;
}

