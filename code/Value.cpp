
#include "Value.h"
#include <iostream>

Value :: Value(std::string word)
{
    this->word = word;
    next = NULL;
    prev = NULL;
}
// post: Value is properly initialised with word as value


Value :: ~Value()
{
    if (next != NULL)
    {
        delete next;
        next = NULL;
    }
}
// recursively deletes all Values.


std::string Value :: getText() const
{
    return word;
}
// post: current value is returned


void Value :: setText(std::string value)
{
    word = value;
}
// post: value is updated with new value

Value* Value :: getNext() 
{
    return next;
}
// post: pointer to next value is returned

void Value :: setNext(Value* next)
{
    this->next = next;
}
// post: pointer to next value is set

Value* Value :: getPrev()
{
    return prev;
}
// post: pointer to prev value is returned

void Value :: setPrev(Value* prev)
{
    this->prev = prev;
}
// post: pointer to prev value is set

void Value :: print() const
{
    std :: cout << word << ", ";
    if (next != NULL)
    {
        next->print();   
    }
}
// post: current value is printed to stdout