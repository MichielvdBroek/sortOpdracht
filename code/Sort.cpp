#include "Sort.h"
#include "Key.h"
#include "Value.h"

#include <pthread.h>

template <typename valueT>
int Sort :: getLength(valueT* head, valueT** end)
{
    if (head == NULL || end == NULL)
    {
        throw std:: invalid_argument("head nor end may be NULL");
    }
    int length = 0;
    valueT* temp = head;
    do 
    {
        *end = temp;
        temp = temp->getNext();
        length++;
    } while (temp != NULL);
    return length;
}


//PLACE COMMENTS
template <typename valueT>
valueT* Sort :: mergeTwoLists (valueT* start1, valueT* start2)
{
    valueT* head = NULL;
    valueT* walkHead = NULL;
    valueT* walk1 = start1;
    valueT* walk2 = start2;
    
    if (start1->getText().compare(start2->getText()) < 0)
    {
        head = start1;
        walk1 = walk1->getNext();
    }
    else
    {
        head = start2;
        walk2 = walk2->getNext();
    }
    head->setPrev(NULL);
    walkHead = head;

    while (walk1 != NULL && walk2 != NULL)
    {
        if (walk1->getText().compare(walk2->getText()) < 0)
        {
            walkHead->setNext(walk1);
            walk1->setPrev(walkHead);
            walkHead = walk1;
            walk1 = walk1->getNext();
        }
        else
        {
            walkHead->setNext(walk2);
            walk2->setPrev(walkHead);
            walkHead = walk2;
            walk2 = walk2->getNext();
        }
    }
    if (walk2 != NULL)
    {
        walkHead->setNext(walk2);
        walk2->setPrev(walkHead);
    }
    else
    {
        walkHead->setNext(walk1);
        walk1->setPrev(walkHead);
    }
    return head;
}


template <typename valueT>
valueT* Sort :: sort(valueT* head, valueT* end2, int length)
{
    if (length >= 2)
    {
        valueT* start1 = head;
        valueT* end1 = head;
        int half = length /2;
        valueT* start2 = head;

        for (int i = 0; i < half; i++)
        {
            start2 = start2->getNext();
        }
        end1 = start2->getPrev();

        end1->setNext(NULL);
        if (half >= 2)
        {
            start1 = sort(start1, end1 ,half);
        }
        if (length - half >= 2)
        {
            start2 = sort(start2, end2, length - half);
        }
        return mergeTwoLists(start1, start2);
        //merge lists
    }
    return head;
}


template int Sort :: getLength(Key* head, Key** end);
template int Sort :: getLength(Value* head, Value** end);
template Key* Sort :: mergeTwoLists<Key> (Key* start1, Key* start2);
template Value* Sort :: mergeTwoLists<Value> (Value* start1, Value* start2);
template Key* Sort :: sort(Key* head, Key* end, int length);
template Value* Sort :: sort(Value* head, Value* end, int length);