#pragma once
#include <string>
#include <stdexcept>


class Sort
{
public:

    template <typename valueT>
    static int getLength(valueT* head, valueT** end);

    template <typename valueT>
    static valueT* mergeTwoLists (valueT* start1, valueT* start2);

    template <typename valueT>
    static valueT* sort(valueT* head, valueT* end2, int length);
};

