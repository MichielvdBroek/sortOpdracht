
#include "Key.h"
#include <iostream>

Key :: Key()
{
    nextKey = NULL;
    prevKey = NULL;
    valueHead = NULL;
}
// post: Key is properly initialised, however the key value
//       is illegal (empty)

Key :: ~Key()
{
    if (valueHead != NULL)
    {
        delete valueHead;
    }
    if (nextKey != NULL)
    {
        delete nextKey;
    }
}
// post: recursively deletes all keys and values

std::string Key :: getText() const
{
    return key;
}
// post: current key value is returned

bool Key :: setText(std::string key)
{
    if (key.size() >= 2)
    {
        key.resize(2);
        this->key = key;
    }
    return false;
}
// post: if key length equals 2 the key value is set and true is returned,
//       else key is ignored and false is returned

void Key :: addValue(std::string word)
{
    if (word.length() < 2)
    {
        return;
    }
    if (key == "")
    {
        valueHead = new Value(word);
        setText(word);
    }
    else if (word.compare(0, 2, key) == 0)
    {
        if (valueHead == NULL)
        {
            valueHead = new Value(word);
        }
        else 
        {
            Value* newVal = new Value(word);
            newVal->setNext(valueHead);
            valueHead ->setPrev(newVal);
            valueHead = newVal;
        }
    }
    else if (nextKey == NULL)
    {
        nextKey = new Key();
        nextKey->setPrev(this);
        nextKey->setText(word);
        nextKey->addValue(word);
    }
    else
    {
        nextKey->addValue(word);
    }
}
// post: a new word is added to the correct key:
//       - if the word fits in this key, a new value is added to the valuelist
//       - if the word doesn't fit in this key, addValue is called on the next key
//       - if no fitting key is found, a new key is made with this value in it

Value* Key :: getValuePtr()
{
    return valueHead;
}
// post: pointer to this key's first value is returned

void Key :: setValuePtr(Value* value)
{
    this->valueHead = value;
}
// post: pointer to this key's first value is set

void Key :: setNext(Key* next)
{
    this->nextKey = next;
}
// post: pointer to the next key is set

Key* Key :: getNext()
{
    return nextKey;
}
// post: pointer to the next key is returned

void Key :: setPrev(Key* prev)
{
    this->prevKey = prev;
}
// post: pointer to the prev key is set

Key* Key :: getPrev()
{
    return prevKey;
}
// post: pointer to the prev key is returned

void Key :: print() const
{
    std :: cout << "\n" << key << ": ";
    if (valueHead != NULL)
    {
        valueHead->print();
    }
    if (nextKey != NULL)
    {
        nextKey->print();
    }
}
// post: all keys and values are recursively printed