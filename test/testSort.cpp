#include "../code/Sort.h"
#include "../code/Key.h"
#include "../code/Value.h"
#include <stdexcept>
#include <gtest/gtest.h>
#include <iostream>


class TestSort : public ::testing::Test
{
protected:
    
    TestSort()
    {
        head = NULL;
        s = new Sort();
    }

    virtual ~TestSort() 
    {
        delete s;
    }

    virtual void SetUp()
    {
        //do this in each test
        head = new Key();
        head->addValue("zac");
        head->addValue("aac");
        head->addValue("bac");
        head->addValue("cac");
        head->addValue("aaa");
        head->addValue("aardbij");
        head->addValue("aaedbij");
        head->addValue("aasdbij");
        head->addValue("aagdbij");
        head->addValue("aaydbij");
        head->addValue("aajdbij");
        head->addValue("aawdbij");
        head->addValue("aazdbij");
        head->addValue("aardbij");
        head->addValue("aagdbij");
        head->addValue("anurism");
        head->addValue("dac");
        head->addValue("dirk");
        head->addValue("krachtig");
        head->addValue("eac");
        head->addValue("faalangst");
        head->addValue("beren");
        head->addValue("fac");
        head->addValue("gac");
    }

    virtual void TearDown()
    {
        //do this after each test
        delete head;
        head = NULL;
    }
    Sort* s;
    Key* head;
};

    
    TEST_F(TestSort, testMergeTwoLists)
    {
        Key* list1 = new Key();
        Key* list2 = new Key();

        list1->addValue("bac");
        list1->addValue("dac");
        list1->addValue("eac");

        list2->addValue("aac");
        list2->addValue("cac");
        list2->addValue("fac");

        Key* merged = s->mergeTwoLists<Key>(list1, list2);

        EXPECT_EQ(merged->getText(),                                                                    "aa");
        EXPECT_EQ(merged->getNext()->getText(),                                                         "ba");
        EXPECT_EQ(merged->getNext()->getNext()->getText(),                                              "ca");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getText(),                                   "da");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getText(),                        "ea");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getNext()->getText(),             "fa");
        delete merged;
    }

    TEST_F(TestSort, testMergeTwoSortedLists)
    {
        Key* list1 = new Key();
        Key* list2 = new Key();

        list1->addValue("aac");
        list1->addValue("bac");
        list1->addValue("cac");

        list2->addValue("dac");
        list2->addValue("eac");
        list2->addValue("fac");

        Key* merged = s->mergeTwoLists<Key>(list1, list2);

        EXPECT_EQ(merged->getText(),                                                                    "aa");
        EXPECT_EQ(merged->getNext()->getText(),                                                         "ba");
        EXPECT_EQ(merged->getNext()->getNext()->getText(),                                              "ca");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getText(),                                   "da");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getText(),                        "ea");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getNext()->getText(),             "fa");
        delete merged;
    }

    TEST_F(TestSort, testMergeSortedListsOther)
    {
        Key* list1 = new Key();
        Key* list2 = new Key();

        list1->addValue("dac");
        list1->addValue("eac");
        list1->addValue("fac");

        list2->addValue("aac");
        list2->addValue("bac");
        list2->addValue("cac");

        Key* merged = s->mergeTwoLists<Key>(list1, list2);

        EXPECT_EQ(merged->getText(),                                                                    "aa");
        EXPECT_EQ(merged->getNext()->getText(),                                                         "ba");
        EXPECT_EQ(merged->getNext()->getNext()->getText(),                                              "ca");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getText(),                                   "da");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getText(),                        "ea");
        EXPECT_EQ(merged->getNext()->getNext()->getNext()->getNext()->getNext()->getText(),             "fa");
        delete merged;
    }

    TEST_F(TestSort, testMergeOneItemLists)
    {
        
        Key* list1 = new Key();
        Key* list2 = new Key();

        list1->addValue("aac");
        list2->addValue("bac");

        Key* merged = s->mergeTwoLists<Key>(list1, list2);

        EXPECT_EQ(merged->getText(),            "aa");
        EXPECT_EQ(merged->getNext()->getText(), "ba");
        delete merged;
    }

    TEST_F(TestSort, testSort12Keys)
    {
        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);

        Key* f1 = head->getNext();
        Key* f2 = f1->getNext();
        Key* f3 = f2->getNext();
        Key* f4 = f3->getNext();
        Key* f5 = f4->getNext();
        Key* f6 = f5->getNext();
        Key* f7 = f6->getNext();
        Key* f8 = f7->getNext();
        Key* f9 = f8->getNext();
        Key* f10 = f9->getNext();
        Key* f11 = f10->getNext();

        EXPECT_EQ(head->getText(), "aa");
        EXPECT_EQ(f1->getText(), "an");
        EXPECT_EQ(f2->getText(), "ba");
        EXPECT_EQ(f3->getText(), "be");
        EXPECT_EQ(f4->getText(), "ca");
        EXPECT_EQ(f5->getText(), "da");
        EXPECT_EQ(f6->getText(), "di");
        EXPECT_EQ(f7->getText(), "ea");
        EXPECT_EQ(f8->getText(), "fa");
        EXPECT_EQ(f9->getText(), "ga");
        EXPECT_EQ(f10->getText(), "kr");
        EXPECT_EQ(f11->getText(), "za");
    }

    TEST_F(TestSort, testSort14Keys)
    {
        
        head->addValue("waar");
        head->addValue("gezicht");

        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);

        Key* f1 = head->getNext();
        Key* f2 = f1->getNext();
        Key* f3 = f2->getNext();
        Key* f4 = f3->getNext();
        Key* f5 = f4->getNext();
        Key* f6 = f5->getNext();
        Key* f7 = f6->getNext();
        Key* f8 = f7->getNext();
        Key* f9 = f8->getNext();
        Key* f10 = f9->getNext();
        Key* f11 = f10->getNext();
        Key* f12 = f11->getNext();
        Key* f13 = f12->getNext();

        EXPECT_EQ(head->getText(), "aa");
        EXPECT_EQ(f1->getText(), "an");
        EXPECT_EQ(f2->getText(), "ba");
        EXPECT_EQ(f3->getText(), "be");
        EXPECT_EQ(f4->getText(), "ca");
        EXPECT_EQ(f5->getText(), "da");
        EXPECT_EQ(f6->getText(), "di");
        EXPECT_EQ(f7->getText(), "ea");
        EXPECT_EQ(f8->getText(), "fa");
        EXPECT_EQ(f9->getText(), "ga");
        EXPECT_EQ(f10->getText(), "ge");
        EXPECT_EQ(f11->getText(), "kr");
        EXPECT_EQ(f12->getText(), "wa");
        EXPECT_EQ(f13->getText(), "za");
    }

    TEST_F(TestSort, testSortVals)
    {
        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);

        Key* temp = head;
        for (int i = 0; i < length; i++)
        {
            Value* val = temp->getValuePtr();
            Value* valend = val;
            int vlength = s->getLength(val, &valend);            
            val = s->sort(val, valend, vlength);
            
            temp->setValuePtr(val);
            temp = temp->getNext();
        }

        Value* Values = head->getValuePtr();
        Value* val1 = Values;
        Value* val2 = val1->getNext();
        Value* val3 = val2->getNext();
        Value* val4 = val3->getNext();
        Value* val5 = val4->getNext();
        Value* val6 = val5->getNext();
        Value* val7 = val6->getNext();
        Value* val8 = val7->getNext();
        Value* val9 = val8->getNext();
        Value* val10 = val9->getNext();
        Value* val11 = val10->getNext();
        Value* val12 = val11->getNext();

        EXPECT_EQ(val1->getText(), "aaa");
        EXPECT_EQ(val2->getText(), "aac");
        EXPECT_EQ(val3->getText(), "aaedbij");
        EXPECT_EQ(val4->getText(), "aagdbij");
        EXPECT_EQ(val5->getText(), "aagdbij");
        EXPECT_EQ(val6->getText(), "aajdbij");
        EXPECT_EQ(val7->getText(), "aardbij");
        EXPECT_EQ(val8->getText(), "aardbij");
        EXPECT_EQ(val9->getText(), "aasdbij");
        EXPECT_EQ(val10->getText(), "aawdbij");
        EXPECT_EQ(val11->getText(), "aaydbij");
        EXPECT_EQ(val12->getText(), "aazdbij");
    }
    

    TEST_F(TestSort, testSort12KeysReversed)
    {
        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);
        
        for (int i = 1; i < length; i ++)
        {
            head = head->getNext();
        }

        Key* f1 = head->getPrev();
        Key* f2 = f1->getPrev();
        Key* f3 = f2->getPrev();
        Key* f4 = f3->getPrev();
        Key* f5 = f4->getPrev();
        Key* f6 = f5->getPrev();
        Key* f7 = f6->getPrev();
        Key* f8 = f7->getPrev();
        Key* f9 = f8->getPrev();
        Key* f10 = f9->getPrev();
        Key* f11 = f10->getPrev();

        EXPECT_EQ(head->getText(), "za");
        EXPECT_EQ(f1->getText(), "kr");
        EXPECT_EQ(f2->getText(), "ga");
        EXPECT_EQ(f3->getText(), "fa");
        EXPECT_EQ(f4->getText(), "ea");
        EXPECT_EQ(f5->getText(), "di");
        EXPECT_EQ(f6->getText(), "da");
        EXPECT_EQ(f7->getText(), "ca");
        EXPECT_EQ(f8->getText(), "be");
        EXPECT_EQ(f9->getText(), "ba");
        EXPECT_EQ(f10->getText(), "an");
        EXPECT_EQ(f11->getText(), "aa");
    }

    TEST_F(TestSort, testSort14KeysReversed)
    {
        
        head->addValue("waar");
        head->addValue("gezicht");

        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);

        for (int i = 1; i < length; i ++)
        {
            head = head->getNext();
        }

        Key* f1 = head->getPrev();
        Key* f2 = f1->getPrev();
        Key* f3 = f2->getPrev();
        Key* f4 = f3->getPrev();
        Key* f5 = f4->getPrev();
        Key* f6 = f5->getPrev();
        Key* f7 = f6->getPrev();
        Key* f8 = f7->getPrev();
        Key* f9 = f8->getPrev();
        Key* f10 = f9->getPrev();
        Key* f11 = f10->getPrev();
        Key* f12 = f11->getPrev();
        Key* f13 = f12->getPrev();

        EXPECT_EQ(head->getText(), "za");
        EXPECT_EQ(f1->getText(), "wa");
        EXPECT_EQ(f2->getText(), "kr");
        EXPECT_EQ(f3->getText(), "ge");
        EXPECT_EQ(f4->getText(), "ga");
        EXPECT_EQ(f5->getText(), "fa");
        EXPECT_EQ(f6->getText(), "ea");
        EXPECT_EQ(f7->getText(), "di");
        EXPECT_EQ(f8->getText(), "da");
        EXPECT_EQ(f9->getText(), "ca");
        EXPECT_EQ(f10->getText(), "be");
        EXPECT_EQ(f11->getText(), "ba");
        EXPECT_EQ(f12->getText(), "an");
        EXPECT_EQ(f13->getText(), "aa");
    }

    TEST_F(TestSort, testSortValsReversed)
    {
        Key* end = head;
        int length = s->getLength(head, &end);
        head = s->sort(head, end, length);

        Key* temp = head;
        for (int i = 0; i < length; i++)
        {
            Value* val = temp->getValuePtr();
            Value* valend = val;
            int vlength = s->getLength(val, &valend);            
            val = s->sort(val, valend, vlength);
            
            temp->setValuePtr(val);
            temp = temp->getNext();
        }

        Value* Values = head->getValuePtr();
        Value* vEnd;

        int len = s->getLength(Values, &vEnd);


        for (int i = 1; i < len; i ++)
        {
            Values = Values->getNext();
        }

        Value* val1 = Values;
        Value* val2 = val1->getPrev();
        Value* val3 = val2->getPrev();
        Value* val4 = val3->getPrev();
        Value* val5 = val4->getPrev();
        Value* val6 = val5->getPrev();
        Value* val7 = val6->getPrev();
        Value* val8 = val7->getPrev();
        Value* val9 = val8->getPrev();
        Value* val10 = val9->getPrev();
        Value* val11 = val10->getPrev();
        Value* val12 = val11->getPrev();

        EXPECT_EQ(val12->getText(), "aaa");
        EXPECT_EQ(val11->getText(), "aac");
        EXPECT_EQ(val10->getText(), "aaedbij");
        EXPECT_EQ(val9->getText(), "aagdbij");
        EXPECT_EQ(val8->getText(), "aagdbij");
        EXPECT_EQ(val7->getText(), "aajdbij");
        EXPECT_EQ(val6->getText(), "aardbij");
        EXPECT_EQ(val5->getText(), "aardbij");
        EXPECT_EQ(val4->getText(), "aasdbij");
        EXPECT_EQ(val3->getText(), "aawdbij");
        EXPECT_EQ(val2->getText(), "aaydbij");
        EXPECT_EQ(val1->getText(), "aazdbij");
    }
    
